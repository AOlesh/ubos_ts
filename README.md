## End to End automation tests with @playwright/test (Node.js)

![alt text](./ubos-logo.png)

This repository contains automation test framework written with TypeScript and [Playwright](https://playwright.dev/) and implements Page Object Model Pattern.

If you want to run test locally, please follow these steps:

1. Clone this repository
2. Make sure you have `node.js` installed. If you don't, please visit [official website](https://nodejs.org/en/download/) for instructions 
3. Install 'Playwright Test' test runner for end-to-end tests. Run `npm i -D @playwright/test`
4. Install browsers for 'Playwright Test'. Run     `npx playwright install`

That's it, now you can run tests with `npx playwright test` - it will run test in 3 browsers (chromium, firefox, webkit) in parallel.

If you want to run it in headed mode, then change configuration to `headless: true` in `playwright.config.js`
