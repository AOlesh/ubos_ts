export const servicesPageLocators = {


    service: {

        serviceContainer: 'div.global_sidebar',

        nodeRed: {
            openNodeRedList: 'div.wrapper_left_sidebar > div> div> div> div> div> div',
            addNodeRedBtn: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div > div:nth-child(2)',
            firstNodeRed: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div.bp3-collapse > div > li',

            restartNodeRed: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div.bp3-collapse > div > li:nth-child(1) > div > svg:nth-child(1)',
            
            deleteNodeRedBtn: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div.bp3-collapse > div > li > div > svg:nth-child(2)',
            copyNodeRedUrl: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div.bp3-collapse > div > li > div > span:nth-child(4)',
            confirmDeleteBtn: 'div.wrapper_button > button:nth-child(2)',
 

            newNodeRed: {
                closeNewNodeRedModalWindow: 'span[icon="cross"]',
                projectName: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div:nth-child(3) > div > div > div > div > div:nth-child(3) > div > input',
                systemType: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(2) > div:nth-child(3) > div > div > div > div > div:nth-child(4) > div > select',
                createNewEnvBtn: 'text=Create New Env',
                deleteFirstEnvBtn: 'span[class="delete_env"]',
                keyNewEnv: 'input[placeholder="Key"]',
                valueNewEnv: 'input[placeholder="Value"]',
                createNodeRedBtn: "button:has-text('Create NodeRed')",
            },
        },

        iframe: {
            
            openIframeList: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(3) > div > div:nth-child(1)',
            addIframe: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(3) > div > div:nth-child(2)',
            firstIframe: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(3) > div.bp3-collapse > div > li > span > span',
            deleteIframeBtn: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(3) > div.bp3-collapse > div > li > div > svg:nth-child(1)',
            confirmDeleteBtn: 'div.wrapper_button > button:nth-child(2)',
            

            newIframe: {
                iframeName: 'input[placeholder="Enter Iframe name"]',
                iframeUrl: 'input[placeholder="Enter Url Iframe"]',
                createIframeBtn: 'text=Create Iframe',
            },

            externalApp: {
                frame: 'iframe',
                logoUbos: 'body > header > div > div > div > nav > a > img',

            },
        },

        database: {
            openDatabaseList: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(4) > div > div:nth-child(1)',
            addDatabase: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(4) > div > div:nth-child(2)',
            firstDatabase: ' div.wrapper_left_sidebar > div > div > div > div:nth-child(4) > div.bp3-collapse > div > li > span > span',

            newDatabase: {
                nameDatabase: 'input[placeholder="Enter Database name"]',
                nameUser: 'input[placeholder="Enter User Name"]',

                selectDatabaseType: {
                    typeDatabase: 'div > select',
                    mongo: 'div > select > option[value="mongo"]',
                    postgres: 'div > select > option[value="postgres"]',
                    mysql: 'div > select > option[value="mysql"]',
                },

                tagDatabase: 'input[placeholder="Enter Url Iframe"]',
                passwordDatabase: 'input[placeholder="Enter Password"]',
                externalAccessCheckbox: 'input[type="checkbox"]',
                createDatabaseBtn: 'text=Create Database',

            },
        },

        ui: {
            openUiList: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(5) > div:nth-child(1) > div:nth-child(1)',
            addUiBtn: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(5) > div:nth-child(1) > div:nth-child(2)',
            firstUi: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(5) > div > div > li',
            deleteUiBtn: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(5) > div.bp3-collapse > div > li > div > svg',
            copyUiUrl: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(5) > div.bp3-collapse > div > li > div > span > svg',
            confirmDeleteBtn: 'text=yes',
 

            newUi: {
                closeNewNodeRedModalWindow: 'span[icon="cross"]',
                UiName: 'div.wrapper_left_sidebar > div > div > div > div:nth-child(5) > div:nth-child(3) > div > div > div > div > div:nth-child(3) > div > input',
                createUiBtn: "button:has-text('Create Ui')",
            },
        },



    }


};