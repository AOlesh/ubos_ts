export const sidebarPageLocators = {
    homePage: 'div.wrapper_icon_nav > a',
    services: 'div.wrapper_icon_nav:nth-child(2)',
    analytic: 'div.wrapper_icon_nav:nth-child(3)',
    adminpanel: 'div.wrapper_icon_nav:nth-child(4)',
    };