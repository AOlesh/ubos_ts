export const workspacesPageLocators = {
    
    userName: 'div.user_name',
    

    headerWorkspace:{
        createNewWorkspaceBtn: 'div.header_work_space > div:nth-child(1)  button',
        createNewWorkspaceBigPlus: 'div.header_work_space > div >  div > div:nth-child(4)',
        gettingStartedBtn: 'div.button > a',
        nameNewWorkspace: '#nest-messages_user_name',
        submitBtn: ' button[type=submit]',

    },

    workspacesContainer:{
        createNewWorkspaceSmallPlus: 'div.container_header > div.wrapper_title > div.create > span',
        search: 'div.search > input[type=text]',
        sortCreateDate: 'table > thead > tr > th:nth-child(3) > div',
        nameOfFirtstWorkspace: 'table > tbody > tr:nth-child(1) > td:nth-child(2) span',
        editWorkspace: 'tbody > tr:nth-child(1) > td:nth-child(5) > div > button:nth-child(1)',
        launchWorkspace: 'text=launch',
        deleteWorkspaceBtn: 'tbody > tr:nth-child(1) > td:nth-child(5) > div > button:nth-child(3)',
        confirmDeleteWorkspace: 'text=yes',
        

    },
    

};
