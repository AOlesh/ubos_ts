export const gitHubPageLocators = {
    
    auth: {

        loginBtn: 'div > a[data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in"]',
        loginForm: {
            loginField: 'input[name="login"]',
            passField: 'input[name="password"]',
            signInBtn: 'input[name="commit"]'
        },
    },

    deleteRepoBtn: '#options_bucket > div.Box.color-border-danger > ul > li:nth-child(4) > details > summary',
    confirmField: 'details > details-dialog > div.Box-body.overflow-auto > form > p > input',
    confirmDeleteBtn: 'details > details-dialog > div.Box-body.overflow-auto > form > button',

    ssh: {
        titleField: 'input[name="ssh_key[title]"]',
        keyField: 'dd > textarea',
        addKeyBtn: 'form > p > button',
        passField: '#sudo_password',
        confirmButton: '#sudo > sudo-credential-options > div:nth-child(4) > form > div > div > button',
        deleteSsh: {
            deleteBtn: 'details > summary.btn-sm.btn-danger',
            confirmDelete: 'details > details-dialog > div.Box-body.overflow-auto > form > button'

        }


    },
};
