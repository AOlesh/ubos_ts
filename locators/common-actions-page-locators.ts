export const commonActionsPageLocators = {
    
    appUpdatetModalWindow: 'text="Application updated"',
    loadingModal: 'div > div > div.loader',

    uploadFile : {
        chooseFile: 'input[type=file]',
        okBtn: 'div.wrapper_button > button',
    },
    

};