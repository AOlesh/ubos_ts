export const registerUserPageLocators = {
    
    registerPageTitle: 'h1.text-center',
    firstName: '#firstName',
    lastName: '#lastName',
    email: '#email',
    username: '#username',
    password: '#password',
    confirmPassword: '#password-confirm',
    registerButton: 'div > button',
};