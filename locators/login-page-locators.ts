export const loginPageLocators = {

    loginHeader: 'h1',
    username: '#username',
    password: '#password',
    loginButton: 'button[name=login]',
    registerButton: 'div > a',
};