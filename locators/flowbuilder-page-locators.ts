export const flowBuilderPageLocators = {

frameNodered: 'iframe',

titleNodeRedTab : 'div.drag-initiator div.drag-initiator',

welcomeScreen: {
    closeWelcomeScreenBtn: 'body > div.red-ui-popover.red-ui-tourGuide-popover.red-ui-tourGuide-popover-full.red-ui-popover-inset > div > div > button',
},

reUiHeader: {
    deployButton: '#red-ui-header-button-deploy > span.red-ui-deploy-button-content > span',
},

debug: {
    debugMessages: '#red-ui-tab-debug-link-button',
    msgObject: 'span.red-ui-debug-msg-type-string.red-ui-debug-msg-object-header'

},

NodeRedDialogWindow: {
    noRightNowBtn: 'text =Not right now'

},

redUiWorcspace: {
    redUiWorkspaceChart: '#red-ui-workspace-chart',
    injectNode: {
        injectStart: 'g.red-ui-flow-node-button > rect.red-ui-flow-node-button-button',
        injectOut: 'g.red-ui-flow-node.red-ui-flow-node-group > g.red-ui-flow-port-output > rect'
    },
    debugNode: {
        debugIn: 'g.red-ui-flow-node.red-ui-flow-node-group > g.red-ui-flow-port-input > rect'
    },
    functionNode: {
        function: 'g.red-ui-flow-node.red-ui-flow-node-group > rect.red-ui-flow-node',
        functionIn: 'g.red-ui-flow-node.red-ui-flow-node-group > g.red-ui-flow-port-input',
        functionOut: 'g.red-ui-flow-node.red-ui-flow-node-group > g.red-ui-flow-port-output'
    }

},

editFunctionNode: {
    doneButton: '#node-dialog-ok',
    textContainer : '#node-input-func-editor > div.red-ui-editor-text-container > div > div.overflow-guard > div.monaco-scrollable-element.editor-scrollable.vs > div.lines-content.monaco-editor-background > div.view-lines.monaco-mouse-cursor-text > div:nth-child(1)',

},

paletteCommon: {
    inject: {
        inject:'text=inject'
    },

    debug: {
        debug: '#red-ui-palette-container-common >> text=debug'
    }
},

paletteFunction: {
    function: {
        function: '#red-ui-palette-function >> text=function'
    }
}

};