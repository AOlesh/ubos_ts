export const uiEditorPageLocators = {
    headerRoot: {
        deploySection: {
            deployBtn: 'span > a.t--application-publish-btn > span',
            deployOption: "div.t--deploy-popup-option-trigger",
            connectToGit: 'body > div.bp3-portal > div > div > div > div.bp3-popover-content > div > a.t--connect-to-git-btn',
            currentDeployedVersion: 'div > a.t--current-deployed-preview-btn'
    
        },

    },

    canvas: {
        canvas: 'div[type="CANVAS_WIDGET"]',
        canvasWidgets: {
            canvasAccordionWidget: 'div.t--widget-accordionwidget[data-testid="test-widget"]',
        },
    },

    widgetsSection: {
        switcherWidgets: '#switcher--widgets',
        switcherExplorer: '#switcher--explorer',
        gridWidgets: {
            accordionWidget: 'div[data-guided-tour-id="widget-card-accordionwidget"]',
            audioWidget: 'div[data-guided-tour-id="widget-card-audiowidget"]',
            audiorecorderWidget: 'div[data-guided-tour-id="widget-card-audiorecorderwidget"]',
            bidirectionalchartWidget: 'div[data-guided-tour-id="widget-card-bidirectionalchartwidget"]',
            buttongroupWidget: 'div[data-guided-tour-id="widget-card-buttongroupwidget"]',
            camerawidgetWidget: 'div[data-guided-tour-id="widget-card-camerawidget"]',


        },
    },

    
    gitIntegration: {

        connectGitBtn: 'div.sc-jCQmrR.gnmYyA > span > span > div > a',

        branchList: {
            branchListButton: 'div[data-testid=t--branch-button-container]',
            branchListModal: {
                
                syncBranches: 'span.t--sync-branches',
                closeBranchList: 'span.t--close-branch-list',
                BranchNameField: 'input.branch-search',
                createBranchButton: 'div.t--create-new-branch-button',
                firstBranch: 'div[data-testid=t--git-local-branch-list-container] > div:nth-child(2)',
                firstBranchMoreMenu: 'div[data-testid=t--git-local-branch-list-container] > div:nth-child(2) > span.git-branch-more-menu',
                deleteFirstBranchBtn: 'text=Delete',

                secondBranch: 'div[data-testid=t--git-local-branch-list-container] > div:nth-child(3)',
                firstRemoteBranch: 'div[data-testid=t--git-remote-branch-list-container]  > div:nth-child(2)',
                secondRemoteBranch: 'div[data-testid=t--git-remote-branch-list-container]  > div:nth-child(3)',

            },
        },
        bottomComit: {
            bottomCommitBtn: 'div.t--bottom-bar-commit > div > span.cs-icon.undefined'
        },
        gitModal: {
            closeModalBtn: 'div > div.t--close-git-sync-modal > span[name="close-modal"]',

            gitConnection: {
                repoUrlField: 'input.t--git-repo-input',
            },

            deploy: {
                commitField: 'div.bp3-overlay-enter-done > div > div > div > div > div > div > div:nth-child(5) > div > textarea',
                commitBtn: 'span > span > button.t--commit-button > span',
            },
            
            merge: {

            }

    
        },
    },



};

