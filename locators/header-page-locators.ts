export const headerPageLocators = {

    home: '#root > header > div > a',

    userProfile: {
        userProfileMenu: 'div > div.wrapper_profile > span > span > div',

        userProfileSettings: {
            viewUserEmailOnProfile: 'body > div.bp3-portal div.user-name > span',
            signOutButton: 'text=Sign Out'
        }

    },

};
