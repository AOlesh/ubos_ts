import { expect, Page, Request } from '@playwright/test';
import { uiEditorPageLocators } from '../locators/uieditor-page-locators';
import {workspacesPageLocators} from '../locators/workspaces-page-locators';

export class UiEditorPage {
    readonly page: Page;
    

    constructor(page: Page) {
        this.page = page;
    }

    async open(url: string) {
        await this.page.goto(''+ url);
        await this.page.waitForTimeout(1000);
        return this;
        
    }

    async userIsLoggedIn() {
        let userIsLoggedIn = this.page.locator(workspacesPageLocators.userName);
        await userIsLoggedIn.textContent();
        return userIsLoggedIn;

    }

    async generateGitKey() {
        const deployOption = this.page.frameLocator('iframe').locator(uiEditorPageLocators.headerRoot.deploySection.deployOption);
        deployOption.waitFor({state: 'attached'});
        await deployOption.click();
        const connectGit = this.page.frameLocator('iframe').locator(uiEditorPageLocators.headerRoot.deploySection.connectToGit);
        await connectGit.click();
        const repoUrlField = this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.gitConnection.repoUrlField);
        await repoUrlField.click();
        await this.page.waitForTimeout(2000);
        await repoUrlField.fill(`git@github.com:qaubraine/${global.nameRepo}.git`);
        try{
            //expect(this.page.frameLocator('iframe').locator('text=Generate Key')).toBeTruthy();
            await this.page.frameLocator('iframe').locator('text=Generate Key').click();
        }
        finally{
            await this.page.waitForTimeout(2000);
            await this.page.frameLocator('iframe').locator('span[name=duplicate].t--copy-ssh-key').click();
            return this;
        }
    }

    async connectToRepo(){
        const connectBtn = this.page.frameLocator('iframe').locator('button.t--connect-submit-btn');
        await connectBtn.scrollIntoViewIfNeeded();
        await connectBtn.click();
        expect(this.page.frameLocator('iframe').locator('button.t--commit-button > span')).toBeTruthy();
        const closeBtn = this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.closeModalBtn);
        await closeBtn.click();
        await closeBtn.waitFor({state: 'hidden'});
        await this.page.waitForTimeout(1000);
    }

    async switchToWidgetsGrid() {
        await this.page.frameLocator('iframe').locator(uiEditorPageLocators.widgetsSection.switcherWidgets).click();
    }

    async dragNdrop(sorurceElemrnt: { boundingBox: () => any; }, targetElement: { boundingBox: () => any; }) {
        
        const srcBound = await sorurceElemrnt.boundingBox();
        const trgtBound = await targetElement.boundingBox(); 
  
        const positionBoundX = (srcBound.x + srcBound.width / 2);
        const positionBoundY = (srcBound.y + srcBound.height / 2);
        await this.page.mouse.move(positionBoundX, positionBoundY);
        await this.page.waitForTimeout(500)
        await this.page.mouse.down();
  
        await this.page.waitForTimeout(500);
  
        const positionDistX = (trgtBound.x + trgtBound.width / 2);
        const positionDistY = (trgtBound.y + trgtBound.height / 2);
        await this.page.mouse.move(positionDistX, positionDistY, {steps: 10});
        await this.page.mouse.up();
        return this;
    }

    async commitAndPush() {
        await this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.bottomComit.bottomCommitBtn).click();
        await this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.deploy.commitField).fill('');
        const namecommit = 'Commit' +  Date.now().toString();
        await this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.deploy.commitField).fill(namecommit);
        const [commAndPushBtn] = await Promise.all([
            this.page.waitForResponse(response =>  response.status() === 200),
            this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.deploy.commitBtn).click(),
          ]);
        console.log(commAndPushBtn.status());
        await this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.deploy.commitBtn).waitFor({state: 'hidden'});
        const closeBtn = this.page.frameLocator('iframe').locator(uiEditorPageLocators.gitIntegration.gitModal.closeModalBtn);
        await closeBtn.click();
        await closeBtn.waitFor({state: 'hidden'});
        return this;

    }

}

