import { expect, Page } from '@playwright/test';
import { gitHubPageLocators } from '../locators/git-hub-page-locators'

export class GitHubPage {
    readonly page: Page;
    

    constructor(page: Page) {
        this.page = page;
    }



    async open(gitUrl: string) {
        await this.page.goto(gitUrl);
        expect(this.page.locator('div.Header-item > details > summary img.avatar')).toBeVisible();
        return this;
        
    }

    async createNewRepo() {
        await this.page.waitForSelector('loading-context > div > div.mb-3.Details.js-repos-container.mt-5 > div > h2 > a');
        await this.page.click('loading-context > div > div.mb-3.Details.js-repos-container.mt-5 > div > h2 > a');
        const nameRepo:string = 're' +  Date.now().toString();
        global.nameRepo = nameRepo;
        await this.page.waitForSelector('#repository_name');
        await this.page.fill('#repository_name', nameRepo);
        expect(this.page.locator('#new_repository > div.js-with-permission-fields > button')).toBeVisible();
        await this.page.click('#new_repository > div.js-with-permission-fields > button');
        expect(this.page.url()).toContain(nameRepo);
        await this.page.waitForSelector('div.TableObject-item.TableObject-item--primary > div > span > span > clipboard-copy');
        await this.page.click('div.TableObject-item.TableObject-item--primary > div > span > span > clipboard-copy');
        await this.page.waitForTimeout(2000);
        return this;  
    }

    async deleteRepo() {
        const deleteRepoBtn = this.page.locator(gitHubPageLocators.deleteRepoBtn);
        await deleteRepoBtn.scrollIntoViewIfNeeded();
        await deleteRepoBtn.click();
        await this.page.fill(gitHubPageLocators.confirmField, 'qaubraine/'+global.nameRepo);
        expect(this.page.locator(gitHubPageLocators.confirmDeleteBtn)).toBeVisible();
        await this.page.click(gitHubPageLocators.confirmDeleteBtn);
        try {
            expect(this.page.url()).toContain('?tab=repositories');
            
        } catch(error) {
            await this.page.fill('#sudo_password', 'Runbox55w');
            await this.page.click('#sudo > sudo-credential-options > div:nth-child(4) > form > div > div > button');
        } finally {
            expect(this.page.url()).toContain('?tab=repositories');
            return this;
        }

    }

    async addSshKey(){
        await this.page.waitForSelector(gitHubPageLocators.ssh.titleField);
        await this.page.fill(gitHubPageLocators.ssh.titleField, global.nameRepo);
        await this.page.click(gitHubPageLocators.ssh.keyField);
        await this.page.keyboard.press('Control+V');
        await this.page.click(gitHubPageLocators.ssh.addKeyBtn);
        try{
            expect(this.page.locator(gitHubPageLocators.ssh.passField)).toBeTruthy();
            await this.page.fill(gitHubPageLocators.ssh.passField, 'Runbox55w', {timeout: 10000});
            await this.page.click(gitHubPageLocators.ssh.confirmButton, {timeout: 10000});

        }finally{
            await this.page.waitForSelector(`text=${global.nameRepo}`);
            return this;
        }
    }

    async deleteSshKey(){
        await this.page.locator(gitHubPageLocators.ssh.deleteSsh.deleteBtn).last().click();
        await this.page.locator(gitHubPageLocators.ssh.deleteSsh.confirmDelete).last().click();
        return this;
    }


}

