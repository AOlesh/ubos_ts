import { expect, Page, Request } from '@playwright/test';
import { headerPageLocators } from '../locators/header-page-locators'

export class HeaderPage {
    readonly page: Page;
    

    constructor(page: Page) {
        this.page = page;
    }

    async goHome() {
        await this.page.waitForSelector(headerPageLocators.home);
        return await this.page.click(headerPageLocators.home);
    }


    async goToUserProfile() {
        await this.page.waitForSelector(headerPageLocators.userProfile.userProfileMenu);
        return await this.page.click(headerPageLocators.userProfile.userProfileMenu);
    }


    async logout() {
        await this.page.waitForSelector(headerPageLocators.userProfile.userProfileSettings.signOutButton);
        return await this.page.click(headerPageLocators.userProfile.userProfileSettings.signOutButton);
    }



}