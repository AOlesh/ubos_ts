import { Page, expect } from '@playwright/test';
import { servicesPageLocators } from '../locators/services-page-locators';
import {flowBuilderPageLocators} from '../locators/flowbuilder-page-locators';

export class ServicesPage {
    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }
    
    async addNodeRed(noderedName: string) {
        await this.page.click(servicesPageLocators.service.nodeRed.addNodeRedBtn);
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.newNodeRed.createNodeRedBtn);
        await this.page.fill(servicesPageLocators.service.nodeRed.newNodeRed.projectName, noderedName);
        await this.page.locator(servicesPageLocators.service.nodeRed.newNodeRed.systemType).waitFor({state: 'visible'});
        await this.page.waitForTimeout(3000);
        expect(this.page.locator(servicesPageLocators.service.nodeRed.newNodeRed.systemType)).toContainText('latest');
        const [fbresponse] = await Promise.all([
            this.page.waitForResponse(response =>  response.status() === 200),
            this.page.click(servicesPageLocators.service.nodeRed.newNodeRed.createNodeRedBtn),
          ]);
        console.log(fbresponse.status());
        await this.page.click(servicesPageLocators.service.nodeRed.openNodeRedList);
        return this ;
        
    }


    async addNodeRedwithEnv(noderedName: string, Key: string, Value: string) {
        await this.page.click(servicesPageLocators.service.nodeRed.addNodeRedBtn);
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.newNodeRed.createNodeRedBtn);
        await this.page.fill(servicesPageLocators.service.nodeRed.newNodeRed.projectName, noderedName);
        await this.page.locator(servicesPageLocators.service.nodeRed.newNodeRed.systemType).waitFor({state: 'visible'});
        await this.page.waitForTimeout(3000);
        expect(this.page.locator(servicesPageLocators.service.nodeRed.newNodeRed.systemType)).toContainText('latest');
        await this.page.locator(servicesPageLocators.service.nodeRed.newNodeRed.createNewEnvBtn).click();
        await this.page.fill(servicesPageLocators.service.nodeRed.newNodeRed.keyNewEnv, Key)
        await this.page.fill(servicesPageLocators.service.nodeRed.newNodeRed.valueNewEnv, Value)
        const [fbresponse] = await Promise.all([
            this.page.waitForResponse(response =>  response.status() === 200),
            this.page.click(servicesPageLocators.service.nodeRed.newNodeRed.createNodeRedBtn),
          ]);
        console.log(fbresponse.status());
        await this.page.waitForTimeout(5000);
        await this.page.click(servicesPageLocators.service.nodeRed.openNodeRedList);
        return this ;
    }


    async openNodeRedList() {
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.openNodeRedList);
        await this.page.locator(servicesPageLocators.service.nodeRed.openNodeRedList).first().click();
        return this;
    }

    async openNodeRed() {
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.firstNodeRed);
        await this.page.locator(servicesPageLocators.service.nodeRed.firstNodeRed).first().click();
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).click();
        await this.page.locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).waitFor({state: 'hidden'});
        expect(this.page.locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn)).not.toBeVisible();
        return this;
        
    }

    async DeleteNodeRed() {
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.openNodeRedList);
        await this.page.locator(servicesPageLocators.service.nodeRed.openNodeRedList).first().click();
        await this.page.hover(servicesPageLocators.service.nodeRed.firstNodeRed);
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.deleteNodeRedBtn);
        await this.page.locator(servicesPageLocators.service.nodeRed.deleteNodeRedBtn).click();
        await this.page.locator(servicesPageLocators.service.nodeRed.confirmDeleteBtn).click();
        await this.page.locator(servicesPageLocators.service.nodeRed.confirmDeleteBtn).waitFor({state: 'hidden'});
        expect(this.page.locator(servicesPageLocators.service.nodeRed.confirmDeleteBtn)).not.toBeVisible();
        return this;
        
    }
  
    async copyNodeRedUrl() {
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.firstNodeRed);
        await this.page.hover(servicesPageLocators.service.nodeRed.firstNodeRed);
        await this.page.waitForSelector(servicesPageLocators.service.nodeRed.copyNodeRedUrl);
        await this.page.waitForTimeout(3000);
        await this.page.locator(servicesPageLocators.service.nodeRed.copyNodeRedUrl).click();
        return this;
        
    }

    async addIframe(iframeName: string, iframeUrl: string) {
        await this.page.click(servicesPageLocators.service.iframe.addIframe);
        await this.page.waitForSelector(servicesPageLocators.service.iframe.newIframe.iframeName);
        await this.page.fill(servicesPageLocators.service.iframe.newIframe.iframeName, iframeName);
        await this.page.fill(servicesPageLocators.service.iframe.newIframe.iframeUrl, iframeUrl);
        await this.page.waitForTimeout(1000);
        await this.page.locator(servicesPageLocators.service.iframe.newIframe.createIframeBtn).click();
        await this.page.locator(servicesPageLocators.service.iframe.newIframe.createIframeBtn).waitFor({state: 'hidden'});
        await this.page.waitForTimeout(1000);
        await this.page.click(servicesPageLocators.service.iframe.openIframeList);
        return this;
        
    }

    async openFirstIframe() {
        await this.page.waitForSelector(servicesPageLocators.service.iframe.firstIframe);
        await this.page.locator(servicesPageLocators.service.iframe.firstIframe).click();
        await this.page.waitForTimeout(5000);
        return this;
        
    }


    async DeleteIframe() {

        await this.page.waitForSelector(servicesPageLocators.service.iframe.openIframeList);
        await this.page.locator(servicesPageLocators.service.iframe.openIframeList).first().click();
        await this.page.hover(servicesPageLocators.service.iframe.firstIframe);
        await this.page.waitForSelector(servicesPageLocators.service.iframe.deleteIframeBtn);
        await this.page.locator(servicesPageLocators.service.iframe.deleteIframeBtn).click();
        await this.page.locator(servicesPageLocators.service.iframe.confirmDeleteBtn).click();
        await this.page.locator(servicesPageLocators.service.iframe.confirmDeleteBtn).waitFor({state: 'hidden'});
        return this;
        
    }

    async addDatabaseWithoutExternalAccess(databaseName: string, userName: string, typeDatabase: any, tagDatabase: string, password: string,) {
        await this.page.click(servicesPageLocators.service.database.addDatabase);
        await this.page.waitForSelector(servicesPageLocators.service.database.newDatabase.nameDatabase);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.nameDatabase, databaseName);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.nameUser, userName);
        await this.page.selectOption(servicesPageLocators.service.database.newDatabase.selectDatabaseType.typeDatabase, typeDatabase);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.tagDatabase, tagDatabase);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.passwordDatabase, password);
        return this ;
    }


    async addDatabase(databaseName: string, userName: string, typeDatabase: any, tagDatabase: string, password: string,) {
        await this.page.click(servicesPageLocators.service.database.addDatabase);
        await this.page.waitForSelector(servicesPageLocators.service.database.newDatabase.nameDatabase);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.nameDatabase, databaseName);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.nameUser, userName);
        await this.page.selectOption(servicesPageLocators.service.database.newDatabase.selectDatabaseType.typeDatabase, typeDatabase);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.tagDatabase, tagDatabase);
        await this.page.fill(servicesPageLocators.service.database.newDatabase.passwordDatabase, password);
        await this.page.check(servicesPageLocators.service.database.newDatabase.externalAccessCheckbox);
        expect (await this.page.isChecked(servicesPageLocators.service.database.newDatabase.externalAccessCheckbox)).toBeTruthy();
        await this.page.locator(servicesPageLocators.service.database.newDatabase.createDatabaseBtn).click();
        await this.page.locator(servicesPageLocators.service.database.newDatabase.createDatabaseBtn).waitFor({state: 'hidden'});
        await this.page.click(servicesPageLocators.service.database.openDatabaseList);
        return this ;
        
    }


    async addUI(newNameUI: string) {
        await this.page.click(servicesPageLocators.service.ui.addUiBtn);
        await this.page.waitForSelector(servicesPageLocators.service.ui.newUi.createUiBtn);
        await this.page.fill(servicesPageLocators.service.ui.newUi.UiName, newNameUI);
        const [uiresponse] = await Promise.all([
            this.page.waitForResponse(response =>  response.status() === 200),
            this.page.click(servicesPageLocators.service.ui.newUi.createUiBtn),
          ]);
        console.log(uiresponse.status());
        await this.page.locator(servicesPageLocators.service.ui.newUi.createUiBtn).waitFor({state: 'hidden'});
        await this.page.waitForTimeout(3000);
        await this.page.locator(servicesPageLocators.service.ui.openUiList).click();
        return this ;
        
    }



    async openUI() {
        // await this.page.waitForSelector(servicesPageLocators.service.ui.openUiList);
        // await this.page.click(servicesPageLocators.service.ui.openUiList);
        await this.page.waitForSelector(servicesPageLocators.service.ui.firstUi);
        await this.page.locator(servicesPageLocators.service.ui.firstUi).first().click();
        return this;
    }

    async openUiDelete() {
        await this.page.waitForSelector(servicesPageLocators.service.ui.openUiList);
        await this.page.click(servicesPageLocators.service.ui.openUiList);
        await this.page.waitForSelector(servicesPageLocators.service.ui.firstUi);
        await this.page.locator(servicesPageLocators.service.ui.firstUi).first().click();
        return this;
    }

    async deleteUI() {
        await this.page.waitForSelector(servicesPageLocators.service.ui.firstUi);
        await this.page.hover(servicesPageLocators.service.ui.firstUi);
        await this.page.waitForSelector(servicesPageLocators.service.ui.deleteUiBtn);
        await this.page.locator(servicesPageLocators.service.ui.deleteUiBtn).last().click();
        await this.page.waitForSelector(servicesPageLocators.service.ui.confirmDeleteBtn);
        await this.page.locator(servicesPageLocators.service.ui.confirmDeleteBtn).click();
        await this.page.locator(servicesPageLocators.service.ui.confirmDeleteBtn).waitFor({state: 'hidden'});
        expect(this.page.locator(servicesPageLocators.service.ui.confirmDeleteBtn)).not.toBeVisible();
        return this;

    }


}