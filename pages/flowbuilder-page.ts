import { Page, expect, Locator, Selectors } from '@playwright/test';
import { flowBuilderPageLocators } from '../locators/flowbuilder-page-locators';

export class FlowBuilderPage {
    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }


    async closeWelcomeScreen() {
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).click();
        await this.page.locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).waitFor({state: 'hidden'});
        expect(this.page.locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn)).not.toBeVisible();
        return this ;
    }

    async closeNodeRedDialogWindow() {
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.NodeRedDialogWindow.noRightNowBtn).click();
        await this.page.locator(flowBuilderPageLocators.NodeRedDialogWindow.noRightNowBtn).waitFor({state: 'hidden'});
        expect(this.page.locator(flowBuilderPageLocators.NodeRedDialogWindow.noRightNowBtn)).not.toBeVisible();
        return this ;
    }

    async startFlow() {
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.injectNode.injectStart).first().click();
        expect(this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator('text=Successfully injected: timestamp')).toBeVisible();
        return this;
    }

    async deployButton() {
        const deployButton = this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.reUiHeader.deployButton);
        await deployButton.click();
        expect(this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator('text=Successfully deployed')).toBeVisible();
        return this;
    }

    async debugButton() {
        const debugButton = this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.debug.debugMessages);
        await debugButton.click();
        expect(this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator('#red-ui-tab-debug > a > span')).toBeVisible();
        return this;
    }

    async dragInjection() {
        const UiWorcspace = this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.redUiWorkspaceChart);
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.paletteCommon.inject.inject).first().dragTo(UiWorcspace, {targetPosition: {x: 300, y:250}});
        expect(this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.redUiWorkspaceChart, { hasText: 'timestamp' })).toBeVisible();
        return this;
    }

    async dragDebug() {
        const UiWorcspace = this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.redUiWorkspaceChart);
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.paletteCommon.debug.debug).dragTo(UiWorcspace, {targetPosition: {x: 700, y:450}});
        expect(this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.redUiWorkspaceChart, { hasText: 'debug 1' })).toBeVisible();
        return this;
    }

    async dragFunction() {
        const UiWorcspace = this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.redUiWorkspaceChart);
        await this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.paletteFunction.function.function).dragTo(UiWorcspace, {targetPosition: {x: 500, y:350}});
        expect(this.page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.redUiWorkspaceChart, { hasText: 'function 1' })).toBeVisible();
        return this;
    }

    async dragNdrop(sorurceElemrnt: { boundingBox: () => any; }, targetElement: { boundingBox: () => any; }) {
        const srcBound = await sorurceElemrnt.boundingBox();
        const trgtBound = await targetElement.boundingBox(); 
  
        const positionBoundX = (srcBound.x + srcBound.width / 2);
        const positionBoundY = (srcBound.y + srcBound.height / 2);
        await this.page.mouse.move(positionBoundX, positionBoundY);
        await this.page.waitForTimeout(100)
        await this.page.mouse.down();
  
        await this.page.waitForTimeout(100);
  
        const positionDistX = (trgtBound.x + trgtBound.width / 2);
        const positionDistY = (trgtBound.y + trgtBound.height / 2);
        await this.page.mouse.move(positionDistX, positionDistY, {steps: 10});
        await this.page.mouse.up();
        return this;
    }

}