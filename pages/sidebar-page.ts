import { expect, Page, Request } from '@playwright/test';
import { sidebarPageLocators } from '../locators/sidebar-locators'

export class SidebarPage {
    readonly page: Page;
    

    constructor(page: Page) {
        this.page = page;
    }

    async goHome() {
        await this.page.waitForSelector(sidebarPageLocators.homePage);
        return this.page.click(sidebarPageLocators.homePage);
    }

    async goServices() {
        await this.page.waitForSelector(sidebarPageLocators.services);
        return await this.page.click(sidebarPageLocators.services);
    }

    async goAnalytic() {
        await this.page.waitForSelector(sidebarPageLocators.analytic);
        const analyticIcon = this.page.locator(sidebarPageLocators.analytic);
        await analyticIcon.waitFor({'state': "visible"});
        await this.page.waitForTimeout(5000);
        return await analyticIcon.click();
    }

    async goAdminPanel() {
        await this.page.waitForSelector(sidebarPageLocators.adminpanel);
        const adminPanel = this.page.locator(sidebarPageLocators.adminpanel);
        await adminPanel.waitFor({'state': "visible"});
        await this.page.waitForTimeout(5000);
        return await adminPanel.click();
    }



}