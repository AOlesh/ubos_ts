import { test, expect, APIRequestContext} from '@playwright/test';
import { workspacesPageLocators } from '../locators/workspaces-page-locators';
import { WorkspacesPage } from '../pages/workspaces-page';


test.use({ storageState: "./state.json" });

// Request context is reused by all tests in the file.
let apiContext: APIRequestContext;

test.beforeAll(async ({ playwright, baseURL}) => {
    apiContext = await playwright.request.newContext({
         storageState: 'state.json', baseURL: baseURL
         
    });
})

test.afterAll(async ({ }) => {
  // Dispose all responses.
  await apiContext.dispose();
});


test.describe('Test suite: "Workspace page"', () => {

  test.use({actionTimeout: 20000, navigationTimeout: 10000} );
    test('Workspaces page open', async ({page}) => {
      console.log(Date().toLocaleString());

        // Open the main page of the workspaces and check the display of the user's email on the header
      const workspacespage = new WorkspacesPage(page);
      await workspacespage.open('');
      const userIsLoggedIn = await workspacespage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();
      });
});


test.describe('Test suite: "Workspace header"', () => {

  test.use({actionTimeout: 30000, navigationTimeout: 30000}, )
  //test.skip();


    test('Workspace create wit button', async ({ page }) => {
      console.log(Date().toLocaleString());
      test.setTimeout(60000);

        // Open the main page of the workspaces and check the display of the user's email on the header
      const workspacepage = new WorkspacesPage(page);
      await workspacepage.open('');
      const userIsLoggedIn = await workspacepage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();

      const workspaceName:string = "Workspace create" + Date.now().toString();
      await workspacepage.createNewWorkspace(workspaceName);
      expect(await page.locator('table > tbody > tr > td:nth-child(2)').allTextContents()).toContain(workspaceName);
      
    });


    test('Workspace create with big plus', async ({ page }) => {
      console.log(Date().toLocaleString());
      test.setTimeout(60000);

        // Open the main page of the workspaces and check the display of the user's email on the header
      const workspacepage = new WorkspacesPage(page);
      await workspacepage.open('');
      const userIsLoggedIn = await workspacepage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();

      const workspaceName:string = "Workspace create big plus " + Date.now().toString();
      await workspacepage.createNewWorkspaceBigPlus(workspaceName);
      expect(await page.locator('table > tbody > tr > td:nth-child(2)').allTextContents()).toContain(workspaceName);
      
    });


    test('Getting started open', async ({ page }) => {
      console.log(Date().toLocaleString());
      test.setTimeout(60000);

        // Open the main page of the workspaces and check the display of the user's email on the header
      const workspacepage = new WorkspacesPage(page);
      await workspacepage.open('');
      const userIsLoggedIn = await workspacepage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();

      await workspacepage.gettingstartedOpenBtn();   
      
    });


});


test.describe('Test suite: "Workspace container"', () => {

  test.use({actionTimeout: 30000, navigationTimeout: 30000}, )
  //test.skip();


    test('Workspace create with small plus', async ({ page }) => {
      console.log(Date().toLocaleString());
      test.setTimeout(60000);

        // Open the main page of the workspaces and check the display of the user's email on the header
      const workspacepage = new WorkspacesPage(page);
      await workspacepage.open('');
      const userIsLoggedIn = await workspacepage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();

      const workspaceName:string = "Workspace create small plus " + Date.now().toString();
      await workspacepage.createNewWorkspaceSmallPlus(workspaceName);
      expect(await page.locator('table > tbody > tr > td:nth-child(2)').allTextContents()).toContain(workspaceName);
      
    });


    test('Workspace edit', async ({ page }) => {
      console.log(Date().toLocaleString());
      test.setTimeout(60000);

        // Create a workspace with the name Workspace edit ...
      const NameWorkspace:string = 'Workspace edit' +  Date.now().toString();
      await apiContext.post(`api/v1/organizations`, {
          form: {
            name: NameWorkspace,
          }
      });
        // Opening the main page of the applications and checking the display of the name of the organization
      const workspacepage = new WorkspacesPage(page);
      await workspacepage.open('');
      const userIsLoggedIn = await workspacepage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();


      // Ediit workspace that has name  NameWorkspace
      const current_workspace = page.locator('table > tbody > tr', { hasText: NameWorkspace });
      await current_workspace.waitFor({state: 'visible'});
      const delete_button = current_workspace.locator('button', { hasText: 'edi' });
      await delete_button.click();
      expect(page.url()).toContain('edit');
    
    });


    test('Workspace delete', async ({ page }) => {
      console.log(Date().toLocaleString());
      test.setTimeout(60000);

        // Create a workspace with the name Workspace delete ...
      const NameWorkspace:string = 'Workspace delete' +  Date.now().toString();
      await apiContext.post(`api/v1/organizations`, {
          form: {
            name: NameWorkspace,
          }
      });
        // Opening the main page of the applications and checking the display of the name of the organization
      const workspacepage = new WorkspacesPage(page);
      await workspacepage.open('');
      const userIsLoggedIn = await workspacepage.userIsLoggedIn();
      expect(userIsLoggedIn).toBeTruthy();


      // Deleting workspace that has name  NameWorkspace
      const current_workspace = page.locator('table > tbody > tr', { hasText: NameWorkspace });
      await current_workspace.waitFor({state: 'visible'});
      const delete_button = current_workspace.locator('button', { hasText: 'delete workspace' });
      await delete_button.click();
      await workspacepage.confirmDeleting();
      expect(await page.locator('table > tbody > tr > td:nth-child(2)').allTextContents()).not.toContain(NameWorkspace);
    
    });

});