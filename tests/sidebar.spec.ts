import { test, expect, APIRequestContext} from '@playwright/test';
import { SidebarPage } from '../pages/sidebar-page';
import {WorkspacesPage} from '../pages/workspaces-page'
import {servicesPageLocators} from '../locators/services-page-locators'


test.use({ storageState: "./state.json" });

// Request context is reused by all tests in the file.
let apiContext: APIRequestContext;

test.beforeAll(async ({ playwright, baseURL}) => {
    apiContext = await playwright.request.newContext({
         storageState: 'state.json', baseURL: baseURL
         
    });
})

test.afterAll(async ({ }) => {
  // Dispose all responses.
  await apiContext.dispose();
});





test.describe('Test suite: Side Bar', () => {
    test.use({actionTimeout: 30000, navigationTimeout: 30000}, )
  
    test.skip(({ browserName }) => browserName === 'webkit');
    test.skip(({ browserName }) => browserName === 'firefox');
  
      test('Open Home Page', async ({page}) => {
        test.setTimeout(60000);
  
          // Create a workspace with the random name
        const NameWorkspace:string = 'HomePage of SideBar' +  Date.now().toString();
          //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
          //console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
  
        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();
  
          //switching to Homepage from Sidebar
        const sidebarpage = new SidebarPage(page);
        await sidebarpage.goHome();
        expect(page).toHaveURL('/workspace')
      });

      test('Hide services', async ({page}) => {
        test.setTimeout(60000);
  
          // Create a workspace with the random name
        const NameWorkspace:string = 'Hide Services panel' +  Date.now().toString();
          //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
          //console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
  
        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();
  
          //Hide Services Panel
        const sidebarpage = new SidebarPage(page);
        await sidebarpage.goServices();
        const servicesContainer = page.locator(servicesPageLocators.service.serviceContainer);
        expect(servicesContainer).not.toBeVisible();
  
      });

      test('Open Analytic', async ({page}) => {
        test.setTimeout(60000);
  
          // Create a workspace with the random name
        const NameWorkspace:string = 'Open analytic page' +  Date.now().toString();
          //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
          //console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
  
        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();
  
          //Hide Services Panel
        const sidebarpage = new SidebarPage(page);
        await sidebarpage.goAnalytic();
        const analyticTab = page.locator('div.dock-tab.dock-tab-active');
        await analyticTab.waitFor({'state': 'visible'});
        expect(analyticTab).toHaveText('analytic');
  
      });

      test('Open Admin Panel', async ({page}) => {
        test.setTimeout(60000);
  
          // Create a workspace with the random name
        const NameWorkspace:string = 'Open Admin Panel' +  Date.now().toString();
          //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
          //console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
  
        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();
  
          //Hide Services Panel
        const sidebarpage = new SidebarPage(page);
        await sidebarpage.goAdminPanel();
        const adminPanelTab = page.locator('div.dock-tab.dock-tab-active');
        await adminPanelTab.waitFor({'state': 'visible'});
        expect(adminPanelTab).toHaveText('AdminPanel');
      });
  
  });