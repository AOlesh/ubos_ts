import { test, expect, APIRequestContext } from '@playwright/test';
import { HeaderPage } from '../pages/header-page';
import { headerPageLocators } from '../locators/header-page-locators';
import { WorkspacesPage } from '../pages/workspaces-page';


test.use({ storageState: "./state.json" });

// Request context is reused by all tests in the file.
let apiContext: APIRequestContext;

test.beforeAll(async ({ playwright, baseURL}) => {
    apiContext = await playwright.request.newContext({
         storageState: 'state.json', baseURL: baseURL
         
    });
})

test.afterAll(async ({ }) => {
  // Dispose all responses.
  await apiContext.dispose();
});


  test.describe('Suite: "Header"', () => {
    
    
      test('Open Home page', async ({ page }) => {
        console.log(Date().toLocaleString());
        
        // Create a workspace with the random name
        const NameWorkspace:string = 'Open Home page' +  Date.now().toString();
        //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
        const id = json_response.data.id;
        // console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();


          // Open Home page
        const headerpage = new HeaderPage(page);
        await headerpage.goHome();
        expect(page).toHaveURL('/workspace');

      });

  });      
