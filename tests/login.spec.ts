import { test, expect } from '@playwright/test';

import { user } from '../framework/testdata';
import { headerPageLocators } from '../locators/header-page-locators';
import { loginPageLocators } from '../locators/login-page-locators';
import { WorkspacesPage } from '../pages/workspaces-page';
import { HeaderPage } from '../pages/header-page';
import { LoginPage } from '../pages/login-page';


  test.describe('Suite: "Login and Logout"', () => {
    

    test.use({actionTimeout: 15000} );
    
    
      test('User login and logout', async ({ page }) => {    
        console.log(Date().toLocaleString());  
        
    
        const loginpage = new LoginPage(page);
        await loginpage.open();
        const goToLoginPage = page.locator(loginPageLocators.loginHeader);
        expect(goToLoginPage).toHaveText('Sign in to your account');
    
        await loginpage.login(user.emailForLoginTest, user.password);
        const userIsLoggedIn = await new WorkspacesPage(page).userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();
    
        await new HeaderPage(page).goToUserProfile();
        const onUserProfile = await page.textContent(headerPageLocators.userProfile.userProfileSettings.viewUserEmailOnProfile);
        const userEmail = user.emailForLoginTest;
        expect(onUserProfile).toBe(userEmail);
    
        await new HeaderPage(page).logout();
        const userIsLoggedOut = await new LoginPage(page).userIsLoggedOut();
        expect(userIsLoggedOut).toBeTruthy();
      });
  
  });