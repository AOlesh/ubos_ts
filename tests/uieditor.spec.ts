import { test, expect, APIRequestContext } from '@playwright/test';
import {gitHubPageLocators} from '../locators/git-hub-page-locators';
import { GitHubPage } from '../pages/git-hub-page';
import { ServicesPage } from '../pages/services-page';
import { UiEditorPage } from '../pages/uieditor-page';
import {uiEditorPageLocators} from '../locators/uieditor-page-locators';


test.use({ storageState: "./state.json" });

// Request context is reused by all tests in the file.
let apiContext: APIRequestContext;

test.beforeAll(async ({ playwright, baseURL}) => {
    apiContext = await playwright.request.newContext({
         storageState: 'state.json', baseURL: baseURL
         
    });
});

test.afterAll(async () => {
  // Dispose all responses.
  await apiContext.dispose();
});


  test.describe('Suite: "Git integration', () => {
    
    test.use({
      actionTimeout: 60000, navigationTimeout: 60000
    });

    test.skip(({ browserName }) => browserName === 'webkit');
    test.skip(({ browserName }) => browserName === 'firefox');

      
    test.afterEach(async ({browser}) => {

      // 'UI Editor' deletion after each test in the suite 'Git integration'
      const orgId = global.workspaceId;
      const deleteUi = await apiContext.put('api/UiManager/ui', {
        data: {
          orgId: orgId,
          type: 'ui',
          name: global.newNameUI
        }
      });

      // Repo deletiom after each test in the suite "Git integration"
      const gitHubContext = await browser.newContext({storageState: './gitHubState.json'});
      const gitHubPage = await gitHubContext.newPage();
      const github = new GitHubPage(gitHubPage);
      await github.open('https://github.com/qaubraine/'+global.nameRepo+'/settings');
      await github.deleteRepo();

      // SSH key deletiom after each test in the suite "Git integration"
      await github.open('https://github.com/settings/keys');
      await github.deleteSshKey();
      
    });
      
      test('GitHub connection', async ({browser, page}) => {   
        //test.skip();
        
        console.log(Date().toLocaleString());
        test.setTimeout(900000);

        // Open GitHub profile
        const gitHubContext = await browser.newContext({storageState: './gitHubState.json'});
        const gitHubPage = await gitHubContext.newPage();
        const github = new GitHubPage(gitHubPage);
        await github.open('https://github.com/');
        
        // Create new repo
        await github.createNewRepo();

          // Create a workspace with the random name
        const NameWorkspace:string = 'Git connection' +  Date.now().toString();
          //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
        const workspaceId = json_response.data.id;
        global.workspaceId = workspaceId;
        // console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

        //Create new UI Editor
        const NameUi:string = 'git' +  Date.now().toString();
        let newNameUI = NameUi.slice(0, -2);
        global.newNameUI = newNameUI;
        const orgId = global.workspaceId;
        const createNodeRed = await apiContext.post(`api/UiManager/ui`, {
          data: {
            orgId: orgId,
            name: newNameUI

          }
        });
        expect(createNodeRed.ok()).toBeTruthy();

        // Open the workspace page and check the user's mail display
        const uieditor = new UiEditorPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await uieditor.open(url);
        const userIsLoggedIn = await uieditor.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();

        const servicepage = new ServicesPage(page);
        await page.waitForTimeout(120000);
        await page.reload({waitUntil: 'load'});
        await servicepage.openUiDelete();

        //wait for UI Editor - done
        await page.frameLocator('iframe').locator('#entity-home').waitFor({state: 'visible', timeout: 600000});
        expect(page.frameLocator('iframe').locator('#entity-home')).toBeVisible();

        // // Open an existing UI Editor
        // const uieditor = new UiEditorPage(page);
        // await uieditor.open('https://test.nodered.ubraine.com/workspace/630ef8bddf4fc9120000114e/pages/630ef8bddf4fc9120000114f/edit');
        // await page.frameLocator('iframe').locator('#entity-home').waitFor({state: 'attached', timeout: 600000});
        // expect(page.frameLocator('iframe').locator('#entity-home')).toBeTruthy();

        // Generate SSH Key on UBOS App
        await uieditor.generateGitKey();

        // Add new SSH key into GitHub
        await github.open('https://github.com/settings/ssh/new');
        await github.addSshKey();

        // Connect to GitHub repo
        await uieditor.connectToRepo();

        // Add Accordion widget to canvas
        await uieditor.switchToWidgetsGrid();
        const canvas = page.frameLocator('iframe').locator(uiEditorPageLocators.canvas.canvas);
        const accordionWidgets = page.frameLocator('iframe').locator(uiEditorPageLocators.widgetsSection.gridWidgets.accordionWidget);
        await uieditor.dragNdrop(accordionWidgets, canvas);

        await uieditor.commitAndPush();
        
      });
  
  });

