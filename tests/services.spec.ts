import { test, expect, APIRequestContext} from '@playwright/test';
import { WorkspacesPage } from '../pages/workspaces-page';
import {ServicesPage} from '../pages/services-page';
import {FlowBuilderPage} from '../pages/flowbuilder-page'
import { servicesPageLocators } from '../locators/services-page-locators';
import { flowBuilderPageLocators } from '../locators/flowbuilder-page-locators';
import { user } from '../framework/testdata';
import { CommonActionPage } from '../pages/common-actions-page';
import { commonActionsPageLocators } from '../locators/common-actions-page-locators';


test.use({ storageState: "./state.json" });

// Request context is reused by all tests in the file.
let apiContext: APIRequestContext;

test.beforeAll(async ({ playwright, baseURL}) => {
    apiContext = await playwright.request.newContext({
         storageState: 'state.json', baseURL: baseURL
         
    });
})

test.afterAll(async ({ }) => {
  // Dispose all responses.
  await apiContext.dispose();
});


test.describe('Test suite: Flow Builder', () => {

    test.use({actionTimeout: 300000, navigationTimeout: 300000}, )

    test.skip(({ browserName }) => browserName === 'webkit');
    test.skip(({ browserName }) => browserName === 'firefox');


        // 'Flow Builder' deletion after each test in the suite 'Flow Builder'  
      test.afterEach(async ({}) => {
        const serviceLabel = global.newNameNodeRed + global.appId + 'ubraine'; 
        const orgId = global.workspaceId;
        const deletenodered = await apiContext.post('nodeRedManager/deleteService', {
          form: {
            service_label: serviceLabel,
            orgId: orgId,
          }
        });

        // const response = await deletenodered.json();
        //console.log(response);
        
      });

        test('Create Flow builder', async ({page, baseURL}) => {
          console.log(Date().toLocaleString());
          test.setTimeout(900000);

              // Create a workspace with the random name
            const NameWorkspace:string = 'Create Flow Builder' +  Date.now().toString();
            const newWorkspace = await apiContext.post(`api/v1/organizations`, {
                form: {
                  name: NameWorkspace,
                }
            });

            const json_response = await newWorkspace.json();
            const workspaceId = json_response.data.id;
            const appId = json_response.data.appId;
            const pageId = json_response.data.pageId;

            global.appId = appId;
            global.workspaceId = workspaceId;

            expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

            // Open the workspace page and check the user's mail display
            const workspacepage = new WorkspacesPage(page);
            const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
            await workspacepage.open(url);
            const userIsLoggedIn = await workspacepage.userIsLoggedIn();
            expect(userIsLoggedIn).toBeTruthy();

              //Create new Nodered
            const NameNodeRed:string = 'fb' +  Date.now().toString();
            let newNameNodeRed = NameNodeRed.slice(0, -2);
            const servicepage = new ServicesPage(page);
            await servicepage.addNodeRed(newNameNodeRed);
            global.newNameNodeRed = newNameNodeRed;
            const first_nodered_name = await page.innerText(servicesPageLocators.service.nodeRed.firstNodeRed);
            expect(first_nodered_name).toBe(newNameNodeRed);

              //wait for flow builder - done
            await page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).waitFor({state: 'visible', timeout: 600000});
            expect(page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn)).toBeVisible();

        });

        test('Create Flow Builder with add New Env', async ({page}) => {
          console.log(Date().toLocaleString());
          test.setTimeout(720000);

            // Create a workspace with the random name
          const NameWorkspace:string = 'Create Flow Builder New Env' +  Date.now().toString();
            //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });

          const json_response = await newWorkspace.json();
          const workspaceId = json_response.data.id;
          const appId = json_response.data.appId;
          const pageId = json_response.data.pageId;

          global.appId = appId;
          global.workspaceId = workspaceId;

          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();

            //Create new Flow Builder with add New Env
          const NameNodeRed:string = 'fe' +  Date.now().toString();
          let newNameNodeRed = NameNodeRed.slice(0, -2);
          const Key = 'testKey';
          const Value = 'testValue';
          const servicepage = new ServicesPage(page);
          await servicepage.addNodeRedwithEnv(newNameNodeRed, Key, Value);
          global.newNameNodeRed = newNameNodeRed;
          const first_nodered_name = await page.innerText(servicesPageLocators.service.nodeRed.firstNodeRed);
          expect(first_nodered_name).toBe(newNameNodeRed);

          //wait for flow builder - done
          await page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).waitFor({state: 'visible', timeout: 600000});
          expect(page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn)).toBeVisible();

            //Open first nodered
          await page.locator(servicesPageLocators.service.nodeRed.firstNodeRed).click();
          expect(page.locator(flowBuilderPageLocators.titleNodeRedTab)).toContainText(newNameNodeRed);

            // Create NodeRed flow
          const noderedpage = new FlowBuilderPage(page);
          await noderedpage.closeWelcomeScreen();
          await noderedpage.closeNodeRedDialogWindow();

          await noderedpage.dragInjection();
          await noderedpage.dragFunction();
          const injectionOutput = page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.injectNode.injectOut).first();
          const functionInput = page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.functionNode.functionIn).first();
          await noderedpage.dragNdrop(injectionOutput, functionInput);

          await noderedpage.dragDebug();
          const functionOutput = page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.functionNode.functionOut).last();
          const debugInput = page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.debugNode.debugIn).last();
          await noderedpage.dragNdrop(functionOutput, debugInput);

            // Open function node
          const functionNode = page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.redUiWorcspace.functionNode.function).nth(1);
          await functionNode.dblclick();

            // Fill function node
            const textContainerFunctionNode = page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.editFunctionNode.textContainer).first();
            await textContainerFunctionNode.click();
            await page.waitForTimeout(1000);
            await textContainerFunctionNode.type('let test = env.get("testKey")');
            await page.waitForTimeout(1000);
            await textContainerFunctionNode.press('Enter');
            await page.waitForTimeout(1000);
            await textContainerFunctionNode.type('node.warn(test)');
            await page.waitForTimeout(1000);
            await page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.editFunctionNode.doneButton).click();
            await page.waitForTimeout(5000);
            await noderedpage.deployButton();
            await page.waitForTimeout(5000);
            await noderedpage.debugButton();
            await page.waitForTimeout(5000);
            await noderedpage.startFlow();
            expect(page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.debug.msgObject).last()).toContainText(Value);

        });

        test('Delete the existing Flow Builder', async ({page}) => {
          console.log(Date().toLocaleString());
          test.setTimeout(900000);

            // Create a workspace with the random name
          const NameWorkspace:string = 'Delete exist NodeRed' +  Date.now().toString();
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });
          const json_response = await newWorkspace.json();
          const workspaceId = json_response.data.id;
          const pageId = json_response.data.pageId;
          const appId = json_response.data.appId;
          global.appId = appId;
          global.workspaceId = workspaceId;
          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

            //Create new Nodered
          const NameNodeRed:string = 'na' +  Date.now().toString();
          let newNameNodeRed = NameNodeRed.slice(0, -2);
          global.newNameNodeRed = newNameNodeRed;
          const createNodeRed = await apiContext.post(`nodeRedManager/createNodeRed`, {
            data: {
              orgId: workspaceId,
              application_id: appId,
              project_name: newNameNodeRed,
              nodered_image_tag: "latest",
              env: []

            }
          });
          expect(createNodeRed.ok()).toBeTruthy();

          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();

            //Delete the existing NodeRed
          await page.reload();
          const servicepage = new ServicesPage(page);
          await servicepage.DeleteNodeRed();
          const firstNodered = page.locator(servicesPageLocators.service.nodeRed.firstNodeRed);
          expect(firstNodered).not.toBeVisible();

        });

        test('Check the existing NodeRed', async ({page}) => {
                      //the test not actual more!!!
          test.skip();
          console.log(Date().toLocaleString());
          test.setTimeout(360000);
          
          // Create a workspace with the random name
        const NameWorkspace:string = 'Check NodeRed' +  Date.now().toString();
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const workspaceId = json_response.data.id;
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
        global.appId = appId;
        global.workspaceId = workspaceId;
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          //Create new Nodered
        const NameNodeRed:string = 'na' +  Date.now().toString();
        let newNameNodeRed = NameNodeRed.slice(0, -2);
        global.newNameNodeRed = newNameNodeRed;
        const createNodeRed = await apiContext.post(`nodeRedManager/createNodeRed`, {
          data: {
            application_id: appId,
            project_name: newNameNodeRed,
            nodered_image_tag: "latest",
            env: []

          }
        });
        expect(createNodeRed.ok()).toBeTruthy();

        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();

        //NodeRed status check
        for (let i = 0; i = 30; i++) {
          try {
            if (await page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).isVisible()) {
              break;
            }
            await page.locator(commonActionsPageLocators.loadingModal).waitFor({state: 'hidden', timeout:5000});

          } catch(error) {
            await page.waitForTimeout(5000);
          }
        }

        expect(page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn)).toBeVisible();

        });

        test('Restart the Flow Builder', async ({page}) => {
          test.setTimeout(900000);
          console.log(Date().toLocaleString());
    
            // Create a workspace with the random name
          const NameWorkspace:string = 'Restart exist NodeRed' +  Date.now().toString();
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });
          const json_response = await newWorkspace.json();
          const workspaceId = json_response.data.id;
          const pageId = json_response.data.pageId;
          const appId = json_response.data.appId;
          global.appId = appId;
          global.workspaceId = workspaceId;
          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
    
            //Create new Nodered
          const NameNodeRed:string = 'na' +  Date.now().toString();
          let newNameNodeRed = NameNodeRed.slice(0, -2);
          global.newNameNodeRed = newNameNodeRed;
          const createNodeRed = await apiContext.post(`nodeRedManager/createNodeRed`, {
            data: {
              orgId: workspaceId,
              application_id: appId,
              project_name: newNameNodeRed,
              nodered_image_tag: "latest",
              env: []
    
            }
          });
          expect(createNodeRed.ok()).toBeTruthy();
    
          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();
    
          //NodeRed restart
          const servicepage = new ServicesPage(page);
          await servicepage.openNodeRedList();
          await page.hover(servicesPageLocators.service.nodeRed.firstNodeRed);
          const [restartnodered] = await Promise.all([
            // Waits for the next request with the specified url
            page.waitForRequest('api/v1/restartNodeRed'),
            // Triggers the request
            page.waitForResponse(response => response.json()),
            page.click(servicesPageLocators.service.nodeRed.restartNodeRed)
          ]);
          expect(restartnodered.url()).toContain('api/v1/restartNodeRed');     
    
        });

        test('Open the copied NodeRed url in a new tab', async ({page}) => {
          console.log(Date().toLocaleString());
          //test.fixme();
          test.setTimeout(900000);
            // permissions for work with buffer
          await page.context().grantPermissions(["clipboard-read", "clipboard-write"]); 

            // Create a workspace with the random name
          const NameWorkspace:string = 'Copying URL NodeRed' +  Date.now().toString();
            //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });
          const json_response = await newWorkspace.json();
          const workspaceId = json_response.data.id;
          const pageId = json_response.data.pageId;
          const appId = json_response.data.appId;
          global.appId = appId;
          global.workspaceId = workspaceId;
            //console.log(json_response.data)
          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();

            //Create new Nodered
          const NameNodeRed:string = 'nc' +  Date.now().toString();
          let newNameNodeRed = NameNodeRed.slice(0, -2);
          global.newNameNodeRed = newNameNodeRed;
          const servicepage = new ServicesPage(page);
          await servicepage.addNodeRed(newNameNodeRed);
          const first_nodered_name = await page.innerText(servicesPageLocators.service.nodeRed.firstNodeRed);
          expect(first_nodered_name).toBe(newNameNodeRed);

          //wait for flow builder - done
          await page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn).waitFor({state: 'visible', timeout: 600000});
          expect(page.frameLocator(flowBuilderPageLocators.frameNodered).locator(flowBuilderPageLocators.welcomeScreen.closeWelcomeScreenBtn)).toBeVisible();

            // Open NodeRed in container
          await servicepage.openNodeRed();

            // Copiyng NodeRed URL
          //await servicepage.openNodeRedList();
          await servicepage.copyNodeRedUrl();
            // read the buffer
          const textFromBuffer = await page.evaluate(() => navigator.clipboard.readText());
            // open new page with a copied url 
          const newtab = await page.context().newPage()
          await newtab.goto(textFromBuffer);  
            // check that copied URL contains the name "nodered"
          expect(newtab.url()).toContain(newNameNodeRed);

        });

});


test.describe('Test suite: External App', () => {
  //test.fixme()

  test.use({actionTimeout: 30000, navigationTimeout: 30000}, )

  test.skip(({ browserName }) => browserName === 'webkit');
  test.skip(({ browserName }) => browserName === 'firefox');

    // 'External App' deletion after each test in the suite 'External App'  
    test.afterEach(async ({}) => {
      const deleteIframe = await apiContext.put(`api/v1/applications/` + global.appId + '/global_setting', {
        data: {
          iframe: []
          
        }
      });
      
    });

      test('Test of creating a new External App', async ({page}) => {
        console.log(Date().toLocaleString());
        test.setTimeout(60000);


            // Create a workspace with the random name
          const NameWorkspace:string = 'Create new External App' +  Date.now().toString();
            //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });

          const json_response = await newWorkspace.json();
          const workspaceId = json_response.data.id;
          const appId = json_response.data.appId;
          const pageId = json_response.data.pageId;

          global.appId = appId;
          global.workspaceId = workspaceId;

          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();

            //Create new Iframe
          const NameIframe:string = 'iframe' +  Date.now().toString();
          let newNameIframe = NameIframe.slice(0, -2);
          let iframeUrl = 'https://ubos.tech/'
          const servicepage = new ServicesPage(page);
          await servicepage.addIframe(newNameIframe, iframeUrl);
          const first_iframe_name = await page.innerText(servicesPageLocators.service.iframe.firstIframe);
          expect(first_iframe_name).toBe(newNameIframe);

          // Open first Iframe page
          await servicepage.openFirstIframe();
          const logoUbos = page.frameLocator(servicesPageLocators.service.iframe.externalApp.frame).locator(servicesPageLocators.service.iframe.externalApp.logoUbos);
          expect(logoUbos).toBeVisible();

      });

      test('Test of deleting an existing External App', async ({page}) => {
        console.log(Date().toLocaleString());
        test.setTimeout(60000);

          // Create a workspace with the random name
        const NameWorkspace:string = 'Delete exist External App' +  Date.now().toString();
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const workspaceId = json_response.data.id;
        const appId = json_response.data.appId;
        const pageId = json_response.data.pageId;

        global.appId = appId;
        global.workspaceId = workspaceId;

        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          //create new iframe
        const createIframe = await apiContext.put(`api/v1/applications/` + appId + '/global_setting', {
          data: {
            iframe: [ 
              {
                component: "nodeRed",
                id: "nodeRedId",
                props: "https://ubos.tech",
                title: "UBOS"
              }
            ]
            
          }
        });
        expect(createIframe.ok()).toBeTruthy();

        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();

        //await page.reload();
        const servicepage = new ServicesPage(page);
        await servicepage.DeleteIframe();
        expect(new CommonActionPage(page).appIsUpdated).toBeTruthy();

    });
});


test.describe('Test suite: Databases', () => { 
  //test.skip();

  // 'Mongo Database' deletion after each test in the suite 'Mongo database'  
  test.afterEach(async ({}) => {
    const application_id = global.application_id;
    const id = global.id;
    const service_label = global.service_label + global.databaseName;
    const deletenodered = await apiContext.post('api/v1/deleteDataBase', {
      data: {
        application_id: application_id,
        id: id,
        service_label: service_label,
      }
    });

          
  });


  test.describe('Mongo databases', () => {
    //test.skip();

    test.use({actionTimeout: 30000, navigationTimeout: 30000}, )
  
    test.skip(({ browserName }) => browserName === 'webkit');
    test.skip(({ browserName }) => browserName === 'firefox');

  
      test('Mongo DB without EA', async ({page, baseURL}) => {
        console.log(Date().toLocaleString());
          test.setTimeout(90000);

            // Create a workspace with the random name
          const NameWorkspace:string = 'Create a new Mongo Database' +  Date.now().toString();
            //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });

          const json_response = await newWorkspace.json();
          const workspaceId = json_response.data.id;
          const appId = json_response.data.appId;
          const pageId = json_response.data.pageId;
          
            //console.log(json_response.data)
          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();

            //Create a new Mongo Database without external access
          const NameMongoDatabase:string = 'mongo' +  Date.now().toString();
          let databaseName = NameMongoDatabase.slice(0, -2);
          global.databaseName = databaseName;
          const NameUser:string = 'user' +  Date.now().toString();
          let userName = NameUser.slice(0, -2);
          const typeDatabaseMongo: string = 'mongo';
          const tagDatabase = 'latest';
          const servicepage = new ServicesPage(page);
          await servicepage.addDatabaseWithoutExternalAccess(databaseName, userName, typeDatabaseMongo, tagDatabase, user.password);

          const [dbresponse] = await Promise.all([
            page.waitForResponse(response => response.url() === baseURL + 'api/v1/createDataBase' && response.status() === 200),
            page.click(servicesPageLocators.service.database.newDatabase.createDatabaseBtn),
          ]);
          const datasource_response = await dbresponse.json();
          global.application_id = datasource_response.data.application_id;
          global.id = datasource_response.data.id;
          global.service_label = datasource_response.data.service_label;

          await page.locator(servicesPageLocators.service.database.newDatabase.createDatabaseBtn).waitFor({state: 'hidden'});
          await page.click(servicesPageLocators.service.database.openDatabaseList);
          const first_database_name = await page.innerText(servicesPageLocators.service.database.firstDatabase);
          expect(first_database_name).toBe(databaseName);

      });

  });


  test.describe('Postgres databases', () => {
    //test.skip();

    test.use({actionTimeout: 30000, navigationTimeout: 30000}, )
  
    test.skip(({ browserName }) => browserName === 'webkit');
    test.skip(({ browserName }) => browserName === 'firefox');
  
        test('Postgres DB without EA', async ({page, baseURL}) => {
          console.log(Date().toLocaleString());
            test.setTimeout(90000);
  
              // Create a workspace with the random name
            const NameWorkspace:string = 'Create a new Postgres Database' +  Date.now().toString();
              //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
            const newWorkspace = await apiContext.post(`api/v1/organizations`, {
                form: {
                  name: NameWorkspace,
                }
            });

            const json_response = await newWorkspace.json();
            const workspaceId = json_response.data.id;
            const appId = json_response.data.appId;
            const pageId = json_response.data.pageId;

            expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
  
            // Open the workspace page and check the user's mail display
            const workspacepage = new WorkspacesPage(page);
            const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
            await workspacepage.open(url);
            const userIsLoggedIn = await workspacepage.userIsLoggedIn();
            expect(userIsLoggedIn).toBeTruthy();
  
              //Create a new Postgres Database without external access
            const NamePostgresDatabase:string = 'postgres' +  Date.now().toString();
            let databaseName = NamePostgresDatabase.slice(0, -2);
            global.databaseName = databaseName;
            const NameUser:string = 'user' +  Date.now().toString();
            let userName = NameUser.slice(0, -2);
            const typeDatabaseMongo: string = 'postgres';
            const tagDatabase = 'latest';
            const servicepage = new ServicesPage(page);
            await servicepage.addDatabaseWithoutExternalAccess(databaseName, userName, typeDatabaseMongo, tagDatabase, user.password);

            const [dbresponse] = await Promise.all([
              page.waitForResponse(response => response.url() === baseURL + 'api/v1/createDataBase' && response.status() === 200),
              page.click(servicesPageLocators.service.database.newDatabase.createDatabaseBtn),
            ]);

            const datasource_response = await dbresponse.json();
            global.application_id = datasource_response.data.application_id;
            global.id = datasource_response.data.id;
            global.service_label = datasource_response.data.service_label;

            await page.locator(servicesPageLocators.service.database.newDatabase.createDatabaseBtn).waitFor({state: 'hidden'});
            await page.click(servicesPageLocators.service.database.openDatabaseList);
            const first_database_name = await page.innerText(servicesPageLocators.service.database.firstDatabase);
            expect(first_database_name).toBe(databaseName);
  
        });

  });


  test.describe('Mysql databases', () => {
    // test.skip();

    test.use({actionTimeout: 30000, navigationTimeout: 30000}, )
  
    test.skip(({ browserName }) => browserName === 'webkit');
    test.skip(({ browserName }) => browserName === 'firefox');
  
        test('Mysql DB witout EA', async ({page, baseURL}) => {
          console.log(Date().toLocaleString());
            test.setTimeout(90000);
  
              // Create a workspace with the random name
            const NameWorkspace:string = 'Create a new Mysql Database' +  Date.now().toString();
              //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
            const newWorkspace = await apiContext.post(`api/v1/organizations`, {
                form: {
                  name: NameWorkspace,
                }
            });
            const json_response = await newWorkspace.json();
            const workspaceId = json_response.data.id;
            const appId = json_response.data.appId;
            const pageId = json_response.data.pageId;

            expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});
  
            // Open the workspace page and check the user's mail display
            const workspacepage = new WorkspacesPage(page);
            const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
            await workspacepage.open(url);
            const userIsLoggedIn = await workspacepage.userIsLoggedIn();
            expect(userIsLoggedIn).toBeTruthy();
  
              //Create a new Mysql Database without external access
            const NameMongoDatabase:string = 'mysql' +  Date.now().toString();
            let databaseName = NameMongoDatabase.slice(0, -2);
            global.databaseName = databaseName;
            const NameUser:string = 'user' +  Date.now().toString();
            let userName = NameUser.slice(0, -2);
            const typeDatabaseMongo: string = 'mysql';
            const tagDatabase = 'latest';
            const servicepage = new ServicesPage(page);
            await servicepage.addDatabaseWithoutExternalAccess(databaseName, userName, typeDatabaseMongo, tagDatabase, user.password);

            const [dbresponse] = await Promise.all([
              page.waitForResponse(response => response.url() === baseURL + 'api/v1/createDataBase' && response.status() === 200),
              page.click(servicesPageLocators.service.database.newDatabase.createDatabaseBtn),
            ]);
            const datasource_response = await dbresponse.json();
            global.application_id = datasource_response.data.application_id;
            global.id = datasource_response.data.id;
            global.service_label = datasource_response.data.service_label;

            await page.locator(servicesPageLocators.service.database.newDatabase.createDatabaseBtn).waitFor({state: 'hidden'});
            await page.click(servicesPageLocators.service.database.openDatabaseList);
            const first_database_name = await page.innerText(servicesPageLocators.service.database.firstDatabase);
            expect(first_database_name).toBe(databaseName);
  
        });

  });

});


test.describe('Test suite: UIeditor', () => {
  test.use({actionTimeout: 30000, navigationTimeout: 30000}, )

  test.skip(({ browserName }) => browserName === 'webkit');
  test.skip(({ browserName }) => browserName === 'firefox');


      // 'UI Editor' deletion after each test in the suite 'UI Editor'  
    test.afterEach(async ({page}) => {
      const orgId = global.workspaceId;
      const deleteUi = await apiContext.put('api/UiManager/ui', {
        data: {
          orgId: orgId,
          type: 'ui',
          name: global.newNameUI
        }
      });

      // const response = await deleteUi.json();
      // console.log(response);
      
    });

      test('Create UI Editor', async ({page}) => {
          test.setTimeout(900000);
          console.log(Date().toLocaleString());

            // Create a workspace with the random name
          const NameWorkspace:string = 'Create UI Editor' +  Date.now().toString();
            //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
          const newWorkspace = await apiContext.post(`api/v1/organizations`, {
              form: {
                name: NameWorkspace,
              }
          });
          const json_response = await newWorkspace.json();
          const pageId = json_response.data.pageId;
          const appId = json_response.data.appId;
          const workspaceId = json_response.data.id;
          global.workspaceId = workspaceId;

          // console.log(json_response.data)
          expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

          // Open the workspace page and check the user's mail display
          const workspacepage = new WorkspacesPage(page);
          const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
          await workspacepage.open(url);
          const userIsLoggedIn = await workspacepage.userIsLoggedIn();
          expect(userIsLoggedIn).toBeTruthy();

            //Create new UI
          const NameUI:string = 'ui' +  Date.now().toString();
          let newNameUI = NameUI.slice(0, -2);
          global.newNameUI = newNameUI;
          const servicepage = new ServicesPage(page);
          await servicepage.addUI(newNameUI);
          const first_ui_name = await page.innerText(servicesPageLocators.service.ui.firstUi);
          expect(first_ui_name).toBe(newNameUI);

          //wait for UI Editor - done
          await page.frameLocator('iframe').locator('#entity-home').waitFor({state: 'visible', timeout: 600000});
          expect(page.frameLocator('iframe').locator('#entity-home')).toBeVisible();

      });


      test('Delete the existing UI Editor', async ({page}) => {
        console.log(Date().toLocaleString());
        test.setTimeout(900000);

          // Create a workspace with the random name
        const NameWorkspace:string = 'Delete UI Editor' +  Date.now().toString();
          //let newNameNameWorkspace = NameWorkspace.slice(0, -2);
        const newWorkspace = await apiContext.post(`api/v1/organizations`, {
            form: {
              name: NameWorkspace,
            }
        });
        const json_response = await newWorkspace.json();
        const pageId = json_response.data.pageId;
        const appId = json_response.data.appId;
        const workspaceId = json_response.data.id;
        global.workspaceId = workspaceId;


        // console.log(json_response.data)
        expect(json_response.data).not.toEqual({message: 'This name is already in use', error: true});

        //Create new UI Editor
        const NameUi:string = 'ui' +  Date.now().toString();
        let newNameUI = NameUi.slice(0, -2);
        global.newNameUI = newNameUI;
        const orgId = global.workspaceId;
        const createNodeRed = await apiContext.post(`api/UiManager/ui`, {
          data: {
            orgId: orgId,
            name: newNameUI

          }
        });
        expect(createNodeRed.ok()).toBeTruthy();

        // Open the workspace page and check the user's mail display
        const workspacepage = new WorkspacesPage(page);
        const url = 'workspace/' + appId + '/pages/' + pageId + '/edit';
        await workspacepage.open(url);
        const userIsLoggedIn = await workspacepage.userIsLoggedIn();
        expect(userIsLoggedIn).toBeTruthy();

        const servicepage = new ServicesPage(page);
        //await servicepage.openUiDelete();
        await page.waitForTimeout(120000);
        await page.reload({waitUntil: 'load'});
        await servicepage.openUiDelete();

        //wait for UI Editor - done
        await page.frameLocator('iframe').locator('#entity-home').waitFor({state: 'visible', timeout: 600000});
        expect(page.frameLocator('iframe').locator('#entity-home')).toBeVisible();

          //Deletion the UI Editor
        await servicepage.deleteUI();
        const UiEditor = page.locator(servicesPageLocators.service.ui.firstUi);
        expect(UiEditor).not.toBeVisible();

    });


});