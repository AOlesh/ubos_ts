import { request } from "@playwright/test";
import { Reporter, TestCase, TestResult } from "@playwright/test/reporter";

interface TotalResult { service: string, status: string }

class MyReporter implements Reporter {

  result: TotalResult[] = [] as TotalResult[]

  onTestEnd(test: TestCase, result: TestResult): void {
    this.result.push({ service: test.title, status: result.status })
  }

  async onEnd(): Promise<void> {
    
    const requestContext = await request.newContext();
    const url = 'https://notifier.betedo.com/testNotification';
    //console.log(typeof(this.result));
    console.log(this.result);
    const payload = this.result;
    const msg = await requestContext.post(url, {
        data: {
           payload
        }
    });

    const a = await msg.json();
    console.log(a);


    
  }
}
export default MyReporter;
